import com.bdm.client.ConsoleObserver;
import com.bdm.device.BloodPressureMonitor;
import com.bdm.device.HeartRateMonitor;
import com.bdm.device.IDevice;
import com.bdm.device.TemperatureMonitor;
import com.bdm.observer.IObserver;
import com.bdm.patient.Patient;
import java.util.HashMap;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        var observer1 = new ConsoleObserver("Daughter");
        var observer2 = new ConsoleObserver("Wife");
        List<IObserver> list = List.of(observer1, observer2);

        var device1 = new HeartRateMonitor("HR monitor", list);
        var device2 = new BloodPressureMonitor("BP monitor", list);
        var device3 = new TemperatureMonitor("T monitor", list);

        var hashMap = new HashMap<String, IDevice>();
        hashMap.put(device1.toString(), device1);
        hashMap.put(device2.toString(), device2);
        hashMap.put(device3.toString(), device3);

        var patient = new Patient("John Doe", hashMap);

        for (var i = 0; i < 100; i++) {
            System.out.println("Measurement " + (i + 1));
            patient.measure();
        }
    }
}