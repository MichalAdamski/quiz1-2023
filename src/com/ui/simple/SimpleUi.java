package com.ui.simple;

import com.bdm.client.ConsoleObserver;
import com.bdm.device.BloodPressureMonitor;
import com.bdm.device.HeartRateMonitor;
import com.bdm.device.IDevice;
import com.bdm.device.TemperatureMonitor;
import com.bdm.observer.IObserver;
import com.bdm.patient.Patient;

import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class SimpleUi {
    public static void main(String[] args) {
        var patient = preparePatient();
        var devices = patient.getDevicesNames();
        var scanner = new Scanner(System.in);

        System.out.println("Welcome to patient monitoring system.");

        while (true) {
            System.out.println("Choose device you want to select (q to exit): ");

            for (var i = 0; i < devices.size(); i++) {
                System.out.printf("%d %s\n", i + 1, devices.get(i));
            }

            var input = scanner.nextLine();
            checkIfExit(input);

            try {
                var numericDeviceInput = Integer.parseInt(input.trim());

                if (numericDeviceInput < 1 || numericDeviceInput > devices.size()) {
                    System.out.println("You need to provide number from provided range.");
                    continue;
                }

                System.out.println("Select option (q to exit): ");
                System.out.println("1. start device");
                System.out.println("2. stop device");
                System.out.println("3. measure");

                input = scanner.nextLine();
                checkIfExit(input);

                var numericMethodInput = Integer.parseInt(input.trim());

                if (numericMethodInput < 1 || numericMethodInput > 3) {
                    System.out.println("You need to provide number from provided range.");
                    continue;
                }

                switch (numericMethodInput) {
                    case 1:
                        patient.startDevice(devices.get(numericDeviceInput - 1));
                        break;
                    case 2:
                        patient.stopDevice(devices.get(numericDeviceInput - 1));
                        break;
                    case 3:
                        patient.measure(devices.get(numericDeviceInput - 1));
                        break;
                }
            } catch (Exception e) {
                System.out.println("You need to provide number from provided range.");
            }
        }
    }

    private static void checkIfExit(String input) {
        if (input.equalsIgnoreCase("q")) {
            System.exit(0);
        }
    }

    private static Patient preparePatient() {
        var observer1 = new ConsoleObserver("Daughter");
        var observer2 = new ConsoleObserver("Wife");
        List<IObserver> list = List.of(observer1, observer2);

        var device1 = new HeartRateMonitor("HR monitor", list);
        var device2 = new BloodPressureMonitor("BP monitor", list);
        var device3 = new TemperatureMonitor("T monitor", list);

        var hashMap = new HashMap<String, IDevice>();
        hashMap.put(device1.toString(), device1);
        hashMap.put(device2.toString(), device2);
        hashMap.put(device3.toString(), device3);

        return new Patient("John Doe", hashMap);
    }
}
