package com.bdm.patient;

import com.bdm.device.IDevice;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Patient {
    private final String name;

    private final HashMap<String, IDevice> devices;

    public Patient(String name, HashMap<String, IDevice> devices) {
        this.name = name;
        this.devices = devices;
    }

    public void addDevice(String key, IDevice device) {
        devices.put(key, device);
    }

    public void addDevices(HashMap<String, IDevice> devicesMap) {
        devices.putAll(devicesMap);
    }

    public void measure() {
        for (var device : devices.values()) {
            device.read();
        }
    }

    public void measure(String name) {
        try{
            devices.get(name).read();
        } catch (Exception ignored){}
    }

    public void startDevice(String name) {
        try{
            devices.get(name).start();
        } catch (Exception ignored){}
    }

    public void stopDevice(String name) {
        try{
            devices.get(name).stop();
        } catch (Exception ignored){}
    }

    @Override
    public String toString() {
        return name;
    }

    public List<String> getDevicesNames(){
        return new ArrayList<>(devices.keySet());
    }
}
