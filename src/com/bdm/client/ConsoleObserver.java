package com.bdm.client;

import com.bdm.observer.IObserver;

public class ConsoleObserver implements IObserver {
    private final String name;

    public ConsoleObserver(String name) {
        this.name = name;
    }

    @Override
    public void notify(String message) {
        System.out.printf("Observer %s notified with message: %s%n", name, message);
    }
}
