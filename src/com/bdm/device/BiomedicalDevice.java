package com.bdm.device;

import com.bdm.observer.IObserver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class BiomedicalDevice implements IDevice {
    private final String name;
    private boolean running;
    private final List<IObserver> observers;

    public BiomedicalDevice(String name, List<IObserver> observers) {
        this.name = name;
        this.observers = observers == null ? new ArrayList<>() : observers;
    }

    @Override
    public void start() {
        running = true;
    }

    @Override
    public void stop() {
        running = false;
    }

    @Override
    public boolean isRunning() {
        return running;
    }

    public void addObserver(IObserver observer) {
        observers.add(observer);
    }

    public void addObservers(List<IObserver> observersList) {
        observers.addAll(observersList);
    }

    public void addObservers(IObserver[] observersArray) {
        observers.addAll(Arrays.asList(observersArray));
    }

    protected void notifyObservers(String message) {
        for (var observer : observers) {
            observer.notify(message);
        }
    }

    @Override
    public String toString() {
        return name;
    }
}
