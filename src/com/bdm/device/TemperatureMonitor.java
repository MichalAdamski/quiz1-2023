package com.bdm.device;

import com.bdm.observer.IObserver;

import java.util.List;
import java.util.Random;

public class TemperatureMonitor extends BiomedicalDevice {
    private double temperature;

    public TemperatureMonitor(String name, List<IObserver> observers) {
        super(name, observers);
    }

    @Override
    public boolean isNormal() {
        return temperature > 36 && temperature < 37;
    }

    @Override
    public void read() {
        if(!isRunning()){
            return;
        }

        temperature = (new Random().nextDouble()) * 6 + 34;

        if (!isNormal()) {
            notifyObservers(String.format("Device %s recorded abnormal measurement of body temperature: %.2f C.", this, temperature));
        }
    }
}
