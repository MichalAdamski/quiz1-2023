package com.bdm.device;

import com.bdm.observer.IObserver;

import java.util.List;
import java.util.Random;

public class BloodPressureMonitor extends BiomedicalDevice {
    private int systolic;
    private int diastolic;

    public BloodPressureMonitor(String name, List<IObserver> observers) {
        super(name, observers);
    }

    @Override
    public boolean isNormal() {
        return systolic > 80 && systolic < 120 && diastolic > 50 && diastolic < 80;
    }

    @Override
    public void read() {
        if(!isRunning()){
            return;
        }

        var random = new Random();
        systolic = random.nextInt(120) + 50;
        diastolic = random.nextInt(100) + 20;

        if (!isNormal()) {
            notifyObservers(String.format("Device %s recorded abnormal measurement of pressure %s/%s mmHg.", this, systolic, diastolic));
        }
    }
}
