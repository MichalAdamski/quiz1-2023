package com.bdm.device;

public interface IDevice {
    void start();

    void stop();

    boolean isRunning();

    boolean isNormal();

    void read();
}
