package com.bdm.device;

import com.bdm.observer.IObserver;

import java.util.List;
import java.util.Random;

public class HeartRateMonitor extends BiomedicalDevice {
    private int heartRate;

    public HeartRateMonitor(String name, List<IObserver> observers) {
        super(name, observers);
    }

    @Override
    public boolean isNormal() {
        return heartRate > 40 && heartRate < 120;
    }

    @Override
    public void read() {
        if(!isRunning()){
            return;
        }

        heartRate = (new Random().nextInt(120)) + 20;

        if (!isNormal()) {
            notifyObservers(String.format("Device %s recorded abnormal measurement of heart rate: %s bpm.", this, heartRate));
        }
    }
}
