package com.bdm.observer;

public interface IObserver {
    void notify(String message);
}
